import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService, LoginService]
})
export class LoginComponent implements OnInit {
  loginform: FormGroup;
  versionNumber = '0.1.0';
  Token: any;
  Usertoken: any;
  constructor(private formbuilder: FormBuilder, private messageService: MessageService,
    private login: LoginService, private router: Router) {
   }

  ngOnInit() {
    window.localStorage.clear();
    this.loginform = this.formbuilder.group({
      password: [null, [
        Validators.required,
        Validators.minLength(6)]]
    });
  }


  onSubmit() {
    const data: any = {
        'UserId': '',
        'Password': this.loginform.controls['password'].value
    };
    // const temp = JSON.stringify(data);
    this.login.loginAuth(data).then(res => {
      const TokenData: any = res;
      window.localStorage.setItem('UserToken', TokenData.Token);
      this.Usertoken = window.localStorage.getItem('UserToken');
      if (this.Usertoken !== 'Unauthorized') {
        setTimeout(() => {
    this.router.navigate(['/dashboard/installation']);
     }, 2000);
     this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Login Successful'});
  }
  if (TokenData.Token === 'Unauthorized') {
    this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Invalid Credential'});
  }
     }).catch(err => {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'API ERROR'});
    });
    console.log(data.Password);
  }
}
