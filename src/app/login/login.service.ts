import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
'Accept': 'application/json, text/plain, */*',
'Authorization': 'Basic undefined',
'Content-Type': 'application/json;charset=UTF-8',
'Cookie': 'permissionFlag=true'
  })
};
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public loginAuth(data) {
    const promise = new Promise((resolve, reject) => {
        this.http.post('/api/security/login', data, httpOptions)
        .subscribe(
            success => { resolve(success); },
            error => { reject(error); }
        );
    });
    return promise;
  }
}


// httpOptions.headers.set('Access-Control-Allow-Origin', '*');
// httpOptions.headers.set('Access-Control-Allow-Credentials', 'true');
// httpOptions.headers.set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
// httpOptions.headers.set('Access-Control-Allow-Headers',
// 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method');