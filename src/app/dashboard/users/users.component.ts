import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { DashboardService } from '../dashboard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
export interface UserElement {
  Active: boolean;
  CompanyName: string;
  EMailID: string;
  IsLocked: boolean;
  Name: string;
  Password: string;
  UserCode: string;
}
let updatedObj: any = [];
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [DashboardService]
})
export class UsersComponent implements OnInit {
  AllData: any = [];
  Users: any;
  GUID: string;
  CompName: string;
  constructor(public dboardService: DashboardService, private router: Router, private routeparam: ActivatedRoute) {
  }
  // 'delete'
  displayedColumns = ['select', 'ID', 'NAMN', 'EMAIL', 'TILLÅTELSER', 'AKTIVA', 'LÅSA_LÅSUPP'];
  dataSource = new MatTableDataSource<UserElement>();
  selection = new SelectionModel<UserElement>(true, []);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.routeparam.params.subscribe(params => {
      this.GUID = params['GUID'];
      this.CompName = params['Name'];
    });
    this.getAllUsersdata();
  }
  private makeover(data) {
    if (Array.isArray(data)) {
      updatedObj = [];
      for (let i = 0; i < data.length; i++) {
        if (data != null || data !== []) {
          updatedObj.push({
            'Active': data[i].Active,
            'CompanyName': data[i].CompanyName,
            'EMailID': data[i].EMailID,
            'IsLocked': data[i].IsLocked,
            'Name': data[i].Name,
            'Password': data[i].Password,
            'UserCode': data[i].UserCode,
          });
        }
      }
    }
    return updatedObj;
  }
  /** Get all Data Source for the Users List */
  public getAllUsersdata() {
    this.dboardService.getAllUsersdata(this.GUID)
      .then(res => {
        this.Users = this.makeover(res);
        this.dataSource = new MatTableDataSource<UserElement>(this.Users);
        // this.dataSource =  new MatTableDataSource<UserElement>(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      })
      .catch(err => {
        console.log(err);
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
}