import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { DashboardService } from '../dashboard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
export interface CompanyElement {
  DataBase: string;
  ExtraPath: string;
  GUID: string;
  Name: string;
  SerialNo: string;
  IsActive: boolean;
  ErpSystem: string;
  TolkId: string;
}
let updatedObj: any = [];
@Component({
  selector: 'app-companylist',
  templateUrl: './companylist.component.html',
  styleUrls: ['./companylist.component.css'],
  providers: [DashboardService]
})

export class CompanylistComponent implements OnInit {
  AllData: any = [];
  Installation: any;
  companyName: string;
  serialNo: string;
  constructor(public dboardService: DashboardService, private router: Router, private routeparam: ActivatedRoute) {
  }
  // 'delete'
  displayedColumns = [ 'select', 'Name', 'DataBase', 'TolkId', 'ExtraPath', 'GUID', 'IsActive', 'User'];
  dataSource = new MatTableDataSource<CompanyElement>();
  selection = new SelectionModel<CompanyElement>(true, []);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.routeparam.params.subscribe(params => {
      this.companyName = params['CustomerName'];
      this.serialNo = params['SerialNo'];
    });
      this.getAllCompanydata();
  }
  private makeover(data) {
    if (Array.isArray(data)) {
      updatedObj = [];
      for (let i = 0; i < data.length; i++) {
        if (data != null || data !== []) {
          updatedObj.push({
            'DataBase': data[i].DataBase,
            'ExtraPath': data[i].ExtraPath,
            'GUID': data[i].GUID,
            'Name': data[i].Name,
            'SerialNo': data[i].SerialNo,
            'IsActive': data[i].IsActive,
            'ErpSystem': data[i].ErpSystem,
            'TolkId': data[i].TolkId
          });
        }
      }
    }
    return updatedObj;
  }
  /** Get all Data Source for the Installation List */
  public getAllCompanydata() {
    this.dboardService.getAllCompanydata(this.serialNo)
      .then(res => {
        this.Installation = this.makeover(res);
        this.dataSource = new MatTableDataSource<CompanyElement>(this.Installation);
        // this.dataSource =  new MatTableDataSource<CompanyElement>(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      })
      .catch(err => {
        console.log(err);
      });
  }
  NavigateUserList(item) {
    console.log(item);
    this.router.navigate(['/dashboard/users/', item]);

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
}
