import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { MenuItem } from 'primeng/api';

import { ParentChildCompService } from './../parent-child-comp.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DashboardService]
})
export class DashboardComponent implements OnInit, OnDestroy {
  ConsolidateData: any = [];
  crumbs: MenuItem[];
  constructor(public ParentChild: ParentChildCompService, public dboardService: DashboardService) {
    this.ParentChild.Reload.subscribe( data => {
      this.getConsolidateData();
    });
  }
  ngOnInit() {
      this.getConsolidateData();
    this.crumbs = [
      { label: 'Installationer' },
      { label: 'Kunder' }
    ];
  }
  ngOnDestroy() {
    this.ParentChild.Reload.unsubscribe();
  }
  public getConsolidateData() {
    this.dboardService.getConsolidatedata()
      .then(res => {
        this.ConsolidateData = res;
        console.log(this.ConsolidateData);
      })
      .catch(err => {
        console.log(err);
      });
  }
  Reload() {
    console.log('msg');
  }
}
