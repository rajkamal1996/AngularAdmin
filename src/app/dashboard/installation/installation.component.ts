import { Component, OnInit, ViewChild, Inject, Output, EventEmitter } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { DashboardService } from '../dashboard.service';
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { ParentChildCompService } from '../../parent-child-comp.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MessageService } from 'primeng/api';
export interface PeriodicElement {
  CustomerName: string;
  CustomerNo: number;
  ValidEnd: string;
  NoOfActiveUsers: number;
  SerialNo: number;
  IsActive: boolean;

}

export interface InstanceEntity {
     SerialNo: string;
     CustomerNo: string;
     CustomerName: string;
     ValidEnd: Date;
     IsActive: boolean;
     NoOfActiveUsers: number;
  }
  export interface CompanyEntity {
     GUID: string;
     Name: string;
     Database: string;
     ExtraPath: string;
     SerialNo: string;
  }
  export interface LicenseEntity {
        Code: string;
        FromDate: Date;
        ToDate: Date;
     }
     export interface  UserEntity {
     UserCode: string;
     Name: string;
     EMailID: string;
     Active: boolean;
     Password: string;
  }
  export interface SettingEntity {
     Certificate: boolean;
     Scanning: boolean;
     Transfer: boolean;
     Administrator: boolean;
     Active: boolean;
     Posting: boolean;
     AllInvoice: boolean;
     InvoiceArchive: boolean;
     CompanyName: string;
  }
export interface DialogData {
  animal: string;
  name: string;
}
// const ELEMENT_DATA: PeriodicElement[] = [
//   {
//     'CustomerNo': 123,
//     'CustomerName': 'raj',
//     'ValidEnd': '10 Sep 2018',
//     'NoOfActiveUsers': 31,
//     'SerialNo': 1253432,
//     'IsActive': true
//   }];
let updatedObj: any = [];
@Component({
  selector: 'app-installation',
  templateUrl: './installation.component.html',
  styleUrls: ['./installation.component.scss'],
  providers: [DashboardService],
})
export class InstallationComponent implements OnInit {
  animal: string;
  name: string;

  AllData: any = [];
  Installation: any;
  constructor(public ser: ParentChildCompService, public dboardService: DashboardService, private router: Router,
    public dialog: MatDialog) {
  }
  displayedColumns = ['delete', 'select', 'CustomerNo', 'CustomerName', 'ValidEnd', 'NoOfActiveUsers', 'SerialNo', 'IsActive', 'Kundar'];
  dataSource = new MatTableDataSource<PeriodicElement>();
  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.getAllData();
  }
  public reload() {
    this.getAllData();
    this.ser.RefreshData();
  }
  private makeover(data) {
    if (Array.isArray(data)) {
      updatedObj = [];
      for (let i = 0; i < data.length; i++) {
        if (data != null || data !== []) {
          updatedObj.push({
            'CustomerNo': data[i].CustomerNo,
            'CustomerName': data[i].CustomerName,
            'ValidEnd': data[i].ValidEnd,
            'NoOfActiveUsers': data[i].NoOfActiveUsers,
            'SerialNo': data[i].SerialNo,
            'IsActive': data[i].IsActive
          });
        }
      }
    }
    return updatedObj;
  }
  /** Get all Data Source for the Installation List */
  public getAllData() {
    this.dboardService.getAlldata()
      .then(res => {
        this.Installation = this.makeover(res);
        this.dataSource = new MatTableDataSource<PeriodicElement>(this.Installation);
        // this.dataSource =  new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      })
      .catch(err => {
        console.log(err);
      });
  }
  NavigateCompanyList(item) {
    console.log(item);
    this.router.navigate(['/dashboard/companylist/', item]);

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  openDialog() {
    const dialogRef = this.dialog.open(NewinstallationComponent, {
      // height: '400px',
      // width: '600px',
      data: {name: this.name, animal: this.animal}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('dialog closed');
      this.animal = result;
    });
  }
}
@Component({
  selector: 'app-newinstallation',
  templateUrl: 'newinstallation.component.html',
  styleUrls: ['./newinstallation.component.scss'],
  providers: [MessageService]
})
export class NewinstallationComponent implements OnInit {
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  personal: any;
  GUIDDisable: any;
  ScanlevTabDisable: any;
  VismaAdmin: boolean;
  Scanlev3184300: boolean;
  Fortnox3184306: boolean;
  VismaeKonomi: boolean;
  MultiCompany: boolean;
  API3184309: boolean;
  Interpreted: boolean;
  constructor(
    public dialogRef: MatDialogRef<NewinstallationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InstanceEntity, private _formBuilder: FormBuilder, private messageService: MessageService) {}

    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
        SerialNo: ['{' + this.guid() + '}'],
        CustomerNo: ['', Validators.required],
        CustomerName: ['', Validators.required],
        ValidEnd: ['', Validators.required],
        IsActive: [true , Validators.required]
      });
      this.secondFormGroup = this._formBuilder.group({
        GUID: ['{' + this.guid() + '}'],
        Name:  ['', Validators.required],
        Database: [''],
        ExtraPath: [''],
        IsActive: [true]
      });
      this.thirdFormGroup = this._formBuilder.group({
        Scanlev3184300: [true],
        VismaAdmin: [false],
        VismaeKonomi: [false],
        MultiCompany: [false],
        API3184309: [false],
        Interpreted: [false],
        Fortnox3184306: [false]
      });
      this.fourthFormGroup = this._formBuilder.group({
        Certificate: [true],
        Scanning: [true],
        Transfer: [true],
        Administrator: [true],
        Active: [true],
        Posting: [true],
        AllInvoice: [true],
        InvoiceArchive: [true],
        UserCode: ['', Validators.required],
        Name: ['', Validators.required],
        EMailID: ['', Validators.required],
        Password: [''],
      });
      this.personal = this.firstFormGroup.get('SerialNo');
      this.personal.disable();
      this.GUIDDisable = this.secondFormGroup.get('GUID');
      this.GUIDDisable.disable();
      this.ScanlevTabDisable = this.thirdFormGroup.get('Scanlev3184300');
      this.ScanlevTabDisable.disable();
    }
  OndisabletabClick() {
    this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Cannot change Default Values'});
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

    /**  Generates Unique GUID. */
     guid() {
      return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
      this.s4() + '-' + this.s4() + this.s4() + this.s4();
    }

    // Function to generate serial number and GUID.
     s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
}
