import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
// const usertoken = window.localStorage.getItem('UserToken');
// const httpOptions = {
//   headers: new HttpHeaders({
// 'Accept': 'application/json, text/plain, */*',
// 'Authorization': 'Basic ' + usertoken,
// 'Content-Type': 'application/json;charset=UTF-8',
// 'Cookie': 'permissionFlag=true',
//   })
// };
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  public usertoken = window.localStorage.getItem('UserToken');
  public httpOptions = {
    headers: new HttpHeaders({
  'Accept': 'application/json, text/plain, */*',
  'Authorization': 'Basic ' + this.usertoken,
  'Content-Type': 'application/json;charset=UTF-8',
  'Cookie': 'permissionFlag=true',
    })
  };
  constructor(private http: HttpClient) { }

public getConsolidatedata () {
  const promise = new Promise((resolve, reject) => {
    this.http.get('api/Instance/GetConsolidateData', this.httpOptions)
        .subscribe(success => { resolve(success); }, error => { reject(error); });
});
return promise;
}
// INSTALLATION
public getAlldata () {

  const promise = new Promise((resolve, reject) => {
    this.http.get('api/Instance/GetAll', this.httpOptions)
        .subscribe(success => { resolve(success); }, error => { reject(error); });
});
return promise;
}
// Company List
public getAllCompanydata (sno) {
  const promise = new Promise((resolve, reject) => {
    this.http.get('api/Company/GetAll?SerialNo=' + sno, this.httpOptions)
        .subscribe(success => { resolve(success); }, error => { reject(error); });
});
return promise;
}

// Users List
public getAllUsersdata (GUID) {
  const promise = new Promise((resolve, reject) => {
    this.http.get('api/user/GetListBasedOnCompany?CompanyGUID=' + GUID, this.httpOptions)
        .subscribe(success => { resolve(success); }, error => { reject(error); });
});
return promise;
}}
