import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, NgForm } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRouteModule } from './app.route';
import { routingComponents } from './app.route';
import { LoginComponent } from './login/login.component';
import { ToastModule } from 'primeng/toast';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { MenuItem } from 'primeng/api';
import { ButtonModule } from 'primeng/components/button/button';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CdkTreeModule } from '@angular/cdk/tree';
import {MatInputModule} from '@angular/material';
import { MatTreeModule } from '@angular/material/tree';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { InstallationComponent } from './dashboard/installation/installation.component';
import { CompanylistComponent } from './dashboard/companylist/companylist.component';
import { UsersComponent } from './dashboard/users/users.component';
import { ParentChildCompService } from './parent-child-comp.service';
import { NewinstallationComponent } from './dashboard/installation/installation.component';
import {HttpErrorInterceptor} from './HttpErrorInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    LoginComponent,
    DashboardComponent,
    InstallationComponent,
    CompanylistComponent,
    UsersComponent,
    NewinstallationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRouteModule,
    ReactiveFormsModule,
    ToastModule,
    BreadcrumbModule,
    BrowserAnimationsModule,
    ButtonModule,
    CdkTreeModule,
    MatTreeModule,
    MatInputModule,
    MatTableModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatButtonModule,
    MatDialogModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports: [ NewinstallationComponent ],
  entryComponents: [ NewinstallationComponent ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    ParentChildCompService
  ]
})
export class AppModule { }
