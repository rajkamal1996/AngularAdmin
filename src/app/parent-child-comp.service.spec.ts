import { TestBed, inject } from '@angular/core/testing';

import { ParentChildCompService } from './parent-child-comp.service';

describe('ParentChildCompService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParentChildCompService]
    });
  });

  it('should be created', inject([ParentChildCompService], (service: ParentChildCompService) => {
    expect(service).toBeTruthy();
  }));
});
