import { Injectable, Output, EventEmitter} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParentChildCompService {
@Output() Reload = new EventEmitter<any>();
  constructor() { }
  RefreshData() {
    this.Reload.emit();
  }
}
