import {RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InstallationComponent } from './dashboard/installation/installation.component';
import { CompanylistComponent } from './dashboard/companylist/companylist.component';
import { UsersComponent } from './dashboard/users/users.component';
const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'},
        {path: 'login', component: LoginComponent},
        {path: 'dashboard', component: DashboardComponent,
      children: [
        {path: 'installation', component: InstallationComponent},
        {path: 'companylist', component: CompanylistComponent},
        {path: 'users', component: UsersComponent}
      ]
    }
    ];
  @NgModule({
    imports: [
      RouterModule.forRoot(routes, {
        scrollPositionRestoration: 'enabled',
        anchorScrolling: 'enabled'
      })
    ],
    exports: [
      RouterModule
    ]
  })
    export class AppRouteModule {
    }
    export const routingComponents = [
        LoginComponent,
        DashboardComponent,
        UsersComponent
    ];