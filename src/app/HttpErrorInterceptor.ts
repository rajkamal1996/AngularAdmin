import { Injectable } from "@angular/core";
import { tap } from "rxjs/operators";
import { Router } from '@angular/router';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  public usertoken = window.localStorage.getItem('UserToken');
  constructor(private router: Router) { }
  // function which will be called for all http calls
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // how to update the request Parameters
    // const updatedRequest = request.clone({
    //   headers: request.headers.append('Authorization', 'basic ' + this.usertoken)
    // });
    // logging the updated Parameters to browser's console
    console.log('Before making api call : ', request);
    return next.handle(request).pipe(
      tap(
        event => {
          // logging the http response to browser's console in case of a success
          if (event instanceof HttpResponse) {
            console.log('api call success :', event);
          }
        },
        error => {
          // logging the http response to browser's console in case of a failuer
          if (error instanceof HttpErrorResponse && error.status === 401) {
            if(error.status === 401) {
              this.router.navigate(['/login']);
              location.reload();
            }
            console.log('api call error :', error);
          }
        }
      )
    );
  }
}
