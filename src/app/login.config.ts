import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class LoginConfig implements CanActivate {
  private role;
  constructor() {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const temp = JSON.parse(window.localStorage.getItem('UserToken'));
    return (temp != null) ? true : false;
  }
}